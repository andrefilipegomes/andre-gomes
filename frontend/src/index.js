import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { StateProvider } from './Context/stateProvider';
import reducer, {initialState} from './Context/reducer';
import Header from './Components/Header/Header';

ReactDOM.render(
  <React.StrictMode>
    {/*Data Layer that is wrapping our app giving the ability to push and pull stated data from everywhere*/}
    <StateProvider initialState={initialState} reducer={reducer}>

      {/*Header*/}
      <div className="index__header">
        <Header></Header>
      </div>
      {/* App */}
      <App />
    </StateProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

