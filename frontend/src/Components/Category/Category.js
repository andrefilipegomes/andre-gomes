import '../../Components/Category/Category.css'
import { Chip, makeStyles } from '@material-ui/core'
import React from 'react'
import { useStateValue } from '../../Context/stateProvider';
import { actionTypes } from '../../Context/reducer';

{/*styles used on react components*/}
const useStyles = makeStyles((theme) => ({
    chipRoot: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        listStyle: 'none',
        padding: theme.spacing(0.5),
        margin: 0
    },
    chip: {
        margin: theme.spacing(0.5),
        minWidth: '52.5px'
    },
  }));


function Category({Key, Label}) {
    {/*const that contains the styles declared above*/}
    const classes = useStyles();

    const [ , dispatch] = useStateValue();

    {/*Base function for fetching data*/}
    const baseFetch = (url, requestType, dispatchType) => {
        fetch(url,{
            method: requestType,
            headers: {'Authorization': 'whatever-you-want'},
        })
        .then(res =>{

            {/*Get response json*/}
            res.json().then((dispatchData)=>{

                {/*Get objects from response json dispatch to reducer*/}
                dispatch({
                    type: dispatchType,
                    data: dispatchData
                });

            });
            
        })
        .catch((err)=>{
            alert(err);
        });
    };

    const handleChipClick = (event) =>{
        if(event.target.textContent === 'All'){
            baseFetch('http://localhost:3001/posts', 'GET', actionTypes.SETPOSTS);
        }else{
            baseFetch(`http://localhost:3001/${event.target.textContent}/posts`, 'GET', actionTypes.SETPOSTS);
        }

    };

    return (
        <div className="category">
            <li className="category__chip">
                <Chip
                key={Key}
                onClick={(event) => handleChipClick(event)}
                label={Label}
                className={classes.chip}
                />
            </li>
        </div>
    )
}

export default Category
