import React, { createContext, useContext, useReducer } from 'react';

{/*Creating the context where we are going to store the data layer*/}
export const StateContext = createContext();

{/*
    the 'StateProvider' is the data layer
    'StateContext.Provider' and all is props are use to setup the data layer
    'children' in this case represents the App (the component that is wrapped inside the StateProvider -> in this case see the file index.js)
*/}
export const StateProvider = ({ reducer, initialState, children }) => (
    
    <StateContext.Provider value={useReducer(reducer, initialState)}>
        {children}
    </StateContext.Provider>
);

{/*the useStateValue hook is allowing us to pull data from the data layer*/}
export const useStateValue = () => useContext(StateContext);