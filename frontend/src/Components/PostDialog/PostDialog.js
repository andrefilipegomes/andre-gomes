import { Button, Dialog, DialogActions, DialogContent, DialogTitle, makeStyles, TextareaAutosize, TextField } from '@material-ui/core'
import React, { useState } from 'react'
import { useStateValue } from '../../Context/stateProvider';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { actionTypes } from '../../Context/reducer';
// import AlertDialog from '../Dialog/Dialog';

{/*styles used on react components*/}
const useStyles = makeStyles((theme) => ({
  textArea:{
    marginBottom: '15px',
    marginTop: '27px',
    width: '100%'
  }
}));

function PostDialog({callBackDialogClose, creatingNewPost, openState, postDialogTitle, idValue = '', titleValue = '', authorValue = '', bodyValue = '', categoryValue = ''}) {
    const classes = useStyles();
    
    {/*const that contained the data from the data layer*/}
    const [state, dispatch] = useStateValue();

    // {/*used to manage the state of the PostDialog component*/}
    // const [isPostDialogOpen, setIsPostDialogOpen] = useState((openState)?(openState):(false));

    //  {/*used to manage the state of the Alert Dialog*/}
    //  const [isDialogOpen, setIsDialogOpen] = useState(false);

    {/*used to manage the value of the Title TextField*/}
    const [title, setTitle] = useState(titleValue);

    {/*used to manage the value of the Author TextField*/}
    const [author, setAuthor] = useState(authorValue);

    {/*used to manage the value of the Content TextField*/}
    const [body, setBody] = useState(bodyValue);

    {/*used to manage the value of the Category TextField*/}
    const [category, setCategory] = useState((categoryValue!=='')&&(state.categories.filter(item => item.name === categoryValue)[0]));



    {/*
        Action triggered when one of the inputs is not filled
        this will update the 'isDialogOpen' state to true
    */}
    // const handleDialogOpen = () => {
    //     setIsDialogOpen(true);
    //     setIsPostDialogOpen(false);
    // };

    {/*
        Callback function send to the Dialog component
        this will update the 'isDialogOpen' state to false
    */}
    // const handleDialogClose = () => {
    //     setIsDialogOpen(false);
        
    // };


    {/*
        For creating UUID
        Information:
            https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid/2117523#2117523    
            Answer from @broofa's
    */}
    const uuidv4 = () => {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
          (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }

    {/*
        When the send post button is clicked this is triggered 
        It will create/update the post 
    */}
    const sendPostClick = () =>{ 

        {/*If at least one of the fields is emphty (''), undefined or null it will popup a dialog*/}
        if(title.length > 0 && author.length > 0 && body.length > 0 && category.name.length > 0){
            {/*if we are creating a new post*/}
            if(creatingNewPost === true){
                let newPost ={
                    id: uuidv4(),
                    title: title,
                    author: author,
                    body: body,
                    category: category.name,
                    timestamp: new Date().getTime(),
                    voteScore: 0,
                    commentCount: 0
                }
                
                fetch('http://localhost:3001/posts', 
                    {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json', 'Authorization': 'whatever-you-want' },
                        body: JSON.stringify(newPost)
                    }
                )
                .then(response => { 
                    
                    {/*Add he new post to the data Layer*/}
                    dispatch({
                        type: actionTypes.ADDPOST,
                        data: newPost
                    });
                    callBackDialogClose();
                })
                .catch(error => {
                    alert(error);
                });
            }

            {/*if we are editing a new post*/}
            if(creatingNewPost === false){
                let updatePost ={
                    title: title,
                    body: body
                }
                
                fetch(`http://localhost:3001/posts/${idValue}`, 
                    {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json', 'Authorization': 'whatever-you-want' },
                        body: JSON.stringify(updatePost)
                    }
                )
                .then(response => { 
                    
                    {/*Get objects from response json dispatch to reducer*/}
                    dispatch({
                        type: actionTypes.UPDATEPOST,
                        data: {id: idValue, title: title, body: body}
                    });
                    callBackDialogClose();
                })
                .catch(error => {
                    alert(error);
                });
            }
        }
    };

    {/*When the cancel button is clicked*/}
    const cancelPostClick = () =>{
        //setOpen(false); 
        callBackDialogClose();
    };

    return (
        <div>   
            {
                // (isDialogOpen === true)?(
                //     <PostDialog callBackDialogClose={handleDialogClose} openState={isDialogOpen} postDialogTitle={'Post'}></PostDialog>
                // ):(
                    <Dialog open={openState} onClose={callBackDialogClose} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title">{postDialogTitle}</DialogTitle>
                        <DialogContent>
                            <Autocomplete
                                value={category}
                                onChange={(event, newValue) => {
                                    
                                    if (typeof newValue === "string") {

                                        setCategory(newValue);
                                        
                                    } else if (newValue && newValue.inputValue) 
                                    {

                                        // Create a new value from the user input
                                        setCategory(newValue.inputValue);

                                    } else {
                                        setCategory(newValue);
                                    }
                                }}
                                disabled={!creatingNewPost}
                                selectOnFocus
                                clearOnBlur
                                handleHomeEndKeys
                                id="free-solo-with-text-demo"
                                options={state.categories}
                                getOptionLabel={(option) => {

                                    if(option)
                                        if(option.name)
                                            return option.name;
                                    
                                    return '';
                                    
                                }}
                                renderOption={(option) => option.name}
                                renderInput={(params) => (
                                    <TextField {...params} label="Category*" margin="normal"  />
                                )}
                            />
                            <TextField
                                autoFocus
                                margin="dense"
                                label="Title"
                                fullWidth
                                value={title}
                                onChange={(event) => setTitle(event.target.value)}
                                required={true}
                            />
                            <TextField
                                autoFocus
                                margin="dense"
                                label="Author"
                                disabled={!creatingNewPost}
                                fullWidth
                                value={author}
                                onChange={(event) => setAuthor(event.target.value)}
                                required={true}
                            />
                            <TextareaAutosize 
                                value={body}
                                className={classes.textArea} 
                                rowsMin={3} 
                                rowsMax={10} 
                                placeholder="Content*"
                                onChange={(event) => setBody(event.target.value)}
                            />
                        </DialogContent>
                        <DialogActions>
                        {
                             (creatingNewPost === true)?(
                                <Button type="submit" onClick={() =>  sendPostClick()} color="primary">
                                    Send post 🚀
                                </Button>
                            ):(
                                <Button type="submit" onClick={() =>  sendPostClick()} color="primary">
                                Update post 🚀
                                </Button>
                            )
                        }
                        <Button onClick={() =>  cancelPostClick()} color="primary">
                            Cancel
                        </Button>
                        </DialogActions>
                    </Dialog>
                    
                // )
            }
        </div>
        
    )
}

export default PostDialog
