import React, { useEffect } from 'react'
import './Comments.css'
import Comment from '../../Components/Comment/Comment';
import { makeStyles } from '@material-ui/core';
import { useStateValue } from '../../Context/stateProvider';
import { actionTypes } from '../../Context/reducer';

{/*styles used on react components*/}
const useStyles = makeStyles((theme) => ({
    root:{
        padding: '16px',
        maxHeight: '160px',
        overflowY: 'auto'
    }
}));

function Comments(props) {
    const classes = useStyles();

    const [state, dispatch] = useStateValue();
    
    {/*get comments for specific post from server*/}
    useEffect(()=>{
        {/*this variable will check if the comments of the post in question are already loaded in the data Layer*/}
        let loadedComments = state.comments.find(item => item.parentId === props.postId);

        {/*if the 'loadedComments' doen't exist that means that we need to make a request to the server*/}
        if(!loadedComments){
            fetch(`http://localhost:3001/posts/${props.postId}/comments`,{
                method: "GET",
                headers: {'Authorization': 'whatever-you-want'},
            })
            .then((res)=>{
                 {/*Get response promise*/}
                 res.json().then((commentsList)=>{
                    dispatch({
                        type: actionTypes.SETCOMMENTS,
                        data: commentsList
                    });
                });
            })
            .catch((error)=>{
                alert(error);
            });
        }

    }, []);
    
    return (
        <div className={classes.root}>
            {
                state.comments.filter(comment => comment.parentId === props.postId)?.map((comment) => (
                    <Comment 
                        key={comment.id} 
                        commentId={comment.id}
                        commentAuthor={comment.author}
                        commentBody={comment.body}
                        commentDeleted={comment.deleted}
                        commentParentDeleted={comment.parentDeleted}
                        commentParentId={comment.parentId}
                        commentTimestamp={comment.timestamp}
                        commentVoteScore={comment.voteScore}
                    ></Comment>
                ))
            }
        </div>
    )
}

export default Comments
