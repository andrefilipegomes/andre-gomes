import { Card, CardActions, CardContent, CardHeader, IconButton, makeStyles, Typography } from '@material-ui/core';
import React, { useState } from 'react'
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ThumbsUpDownIcon from '@material-ui/icons/ThumbsUpDown';
import { actionTypes } from '../../Context/reducer';
import { useStateValue } from '../../Context/stateProvider';
import CommentMenu from '../CommentMenu/CommentMenu';


{/*styles used on react components*/}
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        marginBottom: theme.spacing(3),
        border: '1px solid lightgrey'
      },
      avatar: {
        backgroundColor: '#82ccdd'
      },
      cardContent:{
        paddingBottom: '0px'
      },
      body:{
        marginBottom: '10px'
      },
      author:{
        borderBottomStyle: 'double'
      },
      cardActions:{
        padding: '16px'
      },
      positiveBalance:{
        marginLeft: '20px',
        marginRight: '12px',
        color: 'rgb(25, 145, 21)'
      },
      negativeBalance:{
        marginLeft: '20px',
        marginRight: '12px',
        color: 'rgb(95, 14, 14)'
      },
      neutralBalance:{
        marginLeft: '20px',
        marginRight: '12px',
      }
}));

function Comment({commentId, commentAuthor, commentBody, commentDeleted, commentParentDeleted, commentParentId, commentTimestamp, commentVoteScore}) {
    const classes = useStyles();
    
    const [, dispatch] = useStateValue();

    {/*manage state of upVote state*/}
    const [isUpVoteActive, setIsUpVoteActive] = useState(false);

    {/*manage state of upVote state*/}
    const [isDownVoteActive, setIsDownVoteActive] = useState(false);

    {/*Up vote a comment or remove an up vote to 'neutral' state*/}
    const upVoteComment = async () =>{
      let dispatchType;
      let requestOption;

      {/*
        In case the down vote was active we simulate the removal of it
        this way we aplly the up vote to the 'original' voteCount
      */}
      if(isDownVoteActive === true){
        setIsDownVoteActive(false);
        await voteCountUpdateAction("upVote", actionTypes.UPVOTECOMMENT);
      }
        
      
      if(isUpVoteActive === true){
        {/*If the post is already up voted we want to remove it and set to 'neutral' state*/} 
        setIsUpVoteActive(false);
        dispatchType = actionTypes.DOWNVOTECOMMENT;
        requestOption = "downVote";
      }
      else{
        {/*If is at the 'neutral' state we want to up vote*/} 
        setIsUpVoteActive(true);
        dispatchType = actionTypes.UPVOTECOMMENT;
        requestOption = "upVote"
      }

      await voteCountUpdateAction(requestOption, dispatchType);
    };

    {/*Down vote a comment or remove the down vote to 'neutral' state*/}
    const downVoteComment = async () =>{
      let dispatchType;
      let requestOption;

      {/*
        In case the up vote was active we simulate the removal of it
        this way we aplly the down vote to the 'original' voteCount
      */}
      if(isUpVoteActive === true){
        setIsUpVoteActive(false);
        await voteCountUpdateAction("downVote", actionTypes.DOWNVOTECOMMENT);
      }
        
        
      
      
      if(isDownVoteActive === true){
        {/*If we already have a down vote then we want to remove it and set to 'neutral' state*/} 
        setIsDownVoteActive(false);
        dispatchType = actionTypes.UPVOTECOMMENT;
        requestOption = "upVote";
      }
      else{
        {/*If is at the 'neutral' state we want to down vote*/} 
        setIsDownVoteActive(true);
        dispatchType = actionTypes.DOWNVOTECOMMENT;
        requestOption = "downVote";
      }
        
      await voteCountUpdateAction(requestOption, dispatchType);
    };

    const voteCountUpdateAction = async (requestOption, dispatchType) =>{
       
      {/*Fetch to update vote count of post in server*/}
      await fetch(`http://localhost:3001/comments/${commentId}`, 
        {
          method: 'POST', 
          headers: { 'Content-Type': 'application/json', 'Authorization': 'whatever-you-want' },
          body: JSON.stringify({option: requestOption})
        }
      )
      .then(response => { 
          {/*We send the post id so we can update the vote count in the data layer for the intended post */}
          dispatch({
              type: dispatchType,
              data: commentId
          });
      })
      .catch(error => {
          alert(error);
      });

    };

    
    return (
        <div>
            <Card className={classes.root}>
                <CardContent className={classes.cardContent}>
                    <div style={{display: 'flex', marginBottom: '10px'}}>
                        <span>
                            <b style={{marginRight: '10px'}}>
                                {commentAuthor}
                            </b>
                            {commentBody}
                        </span>
                    </div>
                    <Typography className={classes.author} variant="caption" color="textSecondary" component="p">
                        {new Date(commentTimestamp).toLocaleString()}
                    </Typography>
                </CardContent>
                <CardActions className={classes.cardActions} disableSpacing>
                    <IconButton aria-label="Like" onClick={upVoteComment}>
                        <ThumbUpAltIcon style={{color: (isUpVoteActive===true)?("#008000"):("#808080")}}/>
                    </IconButton>
                    <IconButton aria-label="Dislike" onClick={downVoteComment}>
                        <ThumbDownIcon style={{color: (isDownVoteActive===true)?("#FF0000"):("#808080")}}/>
                    </IconButton>
                    {
                    (commentVoteScore > 0)?(
                        <ThumbsUpDownIcon className={classes.positiveBalance}/>
                    ):
                    (commentVoteScore < 0)?(
                        <ThumbsUpDownIcon className={classes.negativeBalance}/>
                    ):
                    (
                        <ThumbsUpDownIcon className={classes.neutralBalance}/>
                    )
                    }
                    <span>{commentVoteScore}</span>

                    <CommentMenu options={['Edit', 'Delete']} commentId={commentId} commentAuthor={commentAuthor} commentBody={commentBody} postId={commentParentId}></CommentMenu>
            
                </CardActions>
            </Card>
        </div>
    )
}

export default Comment
