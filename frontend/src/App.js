import React, { useEffect } from 'react';
import './App.css';
import Categories from './Containers/Categories/Categories';
import Posts from './Containers/Posts/Posts';
import { actionTypes } from './Context/reducer';
import { useStateValue } from './Context/stateProvider';

function App() {
  {/*used to dispatch action to the reducer*/}
  const [ , dispatch] = useStateValue();

  {/*Base function for fetching data*/}
  const baseGet = (url, requestType, dispatchType) => {
 
    fetch(url,{
      method: requestType,
      headers: {'Authorization': 'whatever-you-want'},
      })
      .then(res =>{

        {/*Get response json*/}
        res.json().then((dispatchData)=>{
          {/*Get objects from response json dispatch to reducer*/}
          dispatch({
            type: dispatchType,
            data: dispatchData
          });

        });
        
      })
      .catch((err)=>{
          alert(err);
      });
  };
  
  {/* useAffect that only runs once. The purpose is to get the initial posts that are stored in the serverSide*/}
  useEffect(() => {

    {/*get posts from server*/}
    baseGet("http://localhost:3001/posts", "GET", actionTypes.SETPOSTS);
    
    {/*get categories from server*/}
    baseGet("http://localhost:3001/categories", "GET", actionTypes.SETCATEGORIES);

    
  }, [])

  return (
    <div className="app">
      {/* Menu */}
      <div className="app__headerCategorias">
        <Categories></Categories>
      </div>
      {/*Posts*/}
      <Posts></Posts>
    </div>
  );
}

export default App;
