{/*initialState of the app*/}
export const initialState = {
    postsFilter: [],
    posts: [],
    categories: [],
    comments: []
};

{/* declaration of the actions that can be made in the app for further dispatch (GET, GETBYID, POST, PUT, DELETE, ...) */}
export const actionTypes = {
    SETPOSTS: "SET_POSTS",
    SETCATEGORIES: "SET_CATEGORIES",
    SETCOMMENTS: "SET_COMMENTS",
    FILTERPOSTS: "FILTER_POSTS",
    ADDPOST: "ADD_POST",
    REMOVEPOST: "REMOVE_POST",
    UPDATEPOST: "UPDATE_POST",
    UPVOTEPOST: "UPVOTE_POST",
    DOWNVOTEPOST: "DOWNVOTE_POST",
    UPVOTECOMMENT: "UPVOTE_COMMENT",
    DOWNVOTECOMMENT: "DOWNVOTE_COMMENT",
    REMOVECOMMENT: "REMOVE_COMMENT",
    ADDCOMMENT: "ADD_COMMENT"
}; 

{/*
    the 'switch-case' is determining which request will be made based on the action.type that matches one of the declared actionTypes
    In each 'case' we are going to develop the corresponding request and update the state of the app

    state -> represents the state of the app before the dispatch was made
    action -> has the information of the dispatch made and the data that were sent with it
*/}
const reducer = (state, action) =>{
        // console.log(action);
        // console.log(state);
    let _postsFilterState, newListPosts,item, comment;

    switch(action.type){

        case actionTypes.SETPOSTS:
            
            {/* update posts and postsFilter state based on data received from action */}
            return {
                ...state,
                posts: [...action.data],
                postsFilter: [...action.data]
            };

        case actionTypes.SETCATEGORIES:
            {/* update categories state based on data received from action */}
            return {
                ...state,
                categories: [...state.categories, ...action.data.categories]
            };
        
        case actionTypes.SETCOMMENTS:
            {/* update comments state based on data received from action */}
             
            return {
                ...state,
                comments: [...state.comments, ...action.data]
            };

        case actionTypes.FILTERPOSTS:
            {/*
                update postsFilter state based string received in action
                it will set the state to all elements that contain that specific string in the post title
            */}

            if(action.data){
                {/*If string has a value then we apply the filter*/}
                _postsFilterState = state.posts.filter(x => x.title.toLowerCase().includes(action.data.toLowerCase()));
            }else{
                {/*If string is empty*/}
                _postsFilterState = state.posts;
            }
            
            return{
                ...state,
                postsFilter: _postsFilterState
            };
        
        case actionTypes.ADDPOST:
            {/*
                add new post to postFilter and post
            */}
            return {
                ...state,
                posts: [...state.posts, action.data],
                postsFilter: [...state.postsFilter, action.data]
            };

        case actionTypes.ADDCOMMENT:
             {/*
                add new comment
                and update comment count in the respective post
            */}


            item = state.posts?.find(post => post.id === action.data.parentId);
            
            if(item){
                item.commentCount += 1;
            }

            return {
                ...state,
                comments: [...state.comments, action.data],
                posts: [...state.posts]
            };

        case actionTypes.REMOVEPOST:
            {/*
                remove post by id from the data layer
            */}
            newListPosts = state.posts?.filter((post)=> post.id !== action.data);
            
            return {
                ...state,
                posts: [...newListPosts],
                postsFilter: [...newListPosts]
            };
        
        case actionTypes.REMOVECOMMENT:
        
            {/*
                remove comment
                and update comment count in the respective post
            */}

            item = state.posts?.find(post => post.id === action.data.parentId)

            if(item){
                item.commentCount -= 1;
            }

            newListPosts = state.comments.filter((comment) => comment.id !== action.data.id);
            console.log(newListPosts);
            return {
                ...state,
                comments: [...newListPosts],
                posts: [...state.posts]
            };

        case actionTypes.UPDATEPOST:
            {/*
                update post by id from the data layer
            */}
            
            {/*Get changed post by id from data Layer*/}
            item = state.posts?.find((post)=> post.id === action.data.id);
            
            {/*Update data from changed post*/}
            item.body = action.data.body;
            item.title = action.data.title

            {/*Get all post expect the changed one*/}
            newListPosts = state.posts?.filter((post)=> post.id !== action.data.id);
            
            {/*Update data Layer to contain all posts unchanged and the changed one*/}
            return {
                ...state,
                posts: [...newListPosts, item],
                postsFilter: [...newListPosts, item]
            };

        case actionTypes.UPVOTEPOST:
            {/*
                update vote count by post id
            */}

            {/*Get changed post by id from data Layer*/}
            item = state.posts?.find((post)=> post.id === action.data);
        
            {/*Update post voteCount */}
            item.voteScore += 1;

            {/*Update data Layer to contain all posts unchanged and the changed one*/}
            return {
                ...state,
                posts: [...state.posts],
                postsFilter: [...state.posts]
            };

        case actionTypes.DOWNVOTEPOST:
            {/*
                update vote count by post id
            */}

            {/*Get changed post by id from data Layer*/}
            item = state.posts?.find((post)=> post.id === action.data);
        
            {/*Update post voteCount */}
            item.voteScore -= 1;

            {/*Update data Layer to contain all posts unchanged and the changed one*/}
            return {
                ...state,
                posts: [...state.posts],
                postsFilter: [...state.posts]
            };

        case actionTypes.UPVOTECOMMENT:
            {/*
                update vote count by comment id
            */}

            {/*Get changed comment by id from data Layer*/}
            comment = state.comments?.find((comment)=> comment.id === action.data);
        
            {/*Update comment voteCount */}
            comment.voteScore += 1;

            {/*Update data Layer to contain all comments unchanged and the changed one*/}
            return {
                ...state,
                comments: [...state.comments]
            };

        case actionTypes.DOWNVOTECOMMENT:
            {/*
                update vote count by comment id
            */}

            {/*Get changed comment by id from data Layer*/}
            comment = state.comments?.find((comment)=> comment.id === action.data);
        
            {/*Update comment voteCount */}
            comment.voteScore -= 1;

            {/*Update data Layer to contain all comments unchanged and the changed one*/}
            return {
                ...state,
                comments: [...state.comments]
            };

        default:
            return state;
    }
    
};

export default reducer 