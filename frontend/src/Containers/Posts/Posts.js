import React from 'react';
import Post from '../../Components/Post/Post';
import { useStateValue } from '../../Context/stateProvider';

function Posts() {
    {/*const that contained the data from the data layer*/}
    const [state, dispatch] = useStateValue();
    
    return (
        <div className="posts">
            {
                state.postsFilter?.map((post) => (
                    <Post 
                    key={post.id} 
                    id={post.id} 
                    author={post.author} 
                    body={post.body} 
                    category={post.category} 
                    commentCount={post.commentCount} 
                    deleted={post.deleted} 
                    timestamp={post.timestamp} 
                    title={post.title} 
                    voteScore={post.voteScore} 
                    ></Post>
                ))
            }
        </div>
    )
}

export default Posts





