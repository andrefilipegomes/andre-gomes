import { IconButton, Menu, MenuItem } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import React, { useState } from 'react'
import { actionTypes } from '../../Context/reducer';
import { useStateValue } from '../../Context/stateProvider';
import Post from '../Post/Post';


function CommentMenu({options, commentId, commentAuthor, commentBody, postId}) {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    {/*We have the dispatch so we can delete comments in the data Layer*/}
    const [, dispatch] = useStateValue();

    {/*Used to target the component and open the meun*/}
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    {/*Clear anchor closing the menu*/}
    const handleClose = () => {
      setAnchorEl(null);
    };

    {/*Delete/Edit of comment*/}
    const handleClickMenuItem = (event) =>{
        
      {/*here we delete the comment and remove it from the dataLayer*/}
      if(event.target.textContent === "Delete"){
          fetch(`http://localhost:3001/comments/${commentId}`, 
              {
                  method: 'DELETE',
                  headers: { 'Content-Type': 'application/json', 'Authorization': 'whatever-you-want' }
              }
          )
          .then(response => { 
              {/*We send the comment id so we can remove the comment post from the data layer*/}
              dispatch({
                  type: actionTypes.REMOVECOMMENT,
                  data: {id: commentId, parentId: postId}
              });
              
          })
          .catch(error => {
              alert(error);
          });

          setAnchorEl(null);
      }

      {/**/}
      if(event.target.textContent === "Edit"){
        //   console.log(document.getElementById('TFAuthor').textContent);
        
        // document.getElementById('TFAuthor').textContent = commentAuthor;
        
      }
       
    };

    return (
      <div>
        <IconButton
          aria-label="more"
          aria-controls="long-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          PaperProps={{
            style: {
              width: '20ch',
            },
          }}
        >
          {options.map((menuItem) => (
            <MenuItem key={menuItem} onClick={handleClickMenuItem}>
              {menuItem}
            </MenuItem>
          ))}
        </Menu>
      </div>
    );
}

export default CommentMenu
