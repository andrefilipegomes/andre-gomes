import React, { useState } from 'react';
import { AppBar, fade, InputBase, makeStyles, Toolbar, Typography } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { actionTypes } from '../../Context/reducer';
import { useStateValue } from '../../Context/stateProvider';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { IconButton } from '@material-ui/core';
import PostDialog from '../PostDialog/PostDialog';

{/*styles used on react components*/}
const useStyles = makeStyles((theme) => ({
    header:{
        minHeight:'7vh!important',
        fontSize: '1.1rem'
    },
    title: {
        display: 'block',
        marginRight: '5vw',
        overflow: 'visible'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%!important',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: '60%!important',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%'
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
    }
  }));

function Header() {
    const classes = useStyles();

    {/*used to manage the state of the Dialog*/}
    const [isDialogOpen, setIsDialogOpen] = useState(false);

    {/*used to manage the state of the search input*/}
    const [searchValue, setSearchValue] = useState('');

    {/*used to dispatch action to the reducer*/}
    const [ , dispatch] = useStateValue();

    {/*
        When we interact with the searchBox this will trigger
        It will dispatch an action to update the state of postsFilter
        It will update the seachBox state displaying the typed caracter
    */}
    const searchValueChange = (_searchValue) =>{
      
        dispatch({
            type: actionTypes.FILTERPOSTS,
            data: _searchValue
        })

        setSearchValue(_searchValue);

    };

    {/*
        Action triggered by clicking the + button
        this will update the 'isDialogOpen' state to true
    */}
    const handleDialogOpen = () => {
        setIsDialogOpen(true);
    };

    {/*
        Callback function send to the PostDialog component
        this will update the 'isDialogOpen' state to false
    */}
    const handleDialogClose = () => {
        setIsDialogOpen(false);
    };


    return (
        <div className="header">
            <AppBar position="relative" className={classes.header}>
                <Toolbar className={classes.header}>
                    <Typography className={classes.title} variant="h6" noWrap>
                        Tech Media
                    </Typography>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                        placeholder="Search by title…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        inputProps={{ 'aria-label': 'search' }}
                        value={searchValue}
                        onChange={event => searchValueChange(event.target.value)}
                        />
                    </div>
                    <IconButton style={{marginLeft: 'auto'}} onClick={handleDialogOpen}>
                        <AddCircleIcon style={{ color: '#fafafa', fontSize: 30 }}/>
                    </IconButton>
                    {
                        (isDialogOpen === true)&&(
                            <PostDialog creatingNewPost={true} callBackDialogClose={handleDialogClose} openState={isDialogOpen} postDialogTitle={'New Post'}></PostDialog>
                        )
                    }

                </Toolbar>
            </AppBar>
            
        </div>
    )
}

export default Header
