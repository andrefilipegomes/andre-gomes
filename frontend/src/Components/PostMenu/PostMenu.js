import React from 'react';
import {Menu, MenuItem, IconButton} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useState } from 'react';
import { actionTypes } from '../../Context/reducer';
import { useStateValue } from '../../Context/stateProvider';
import PostDialog from '../PostDialog/PostDialog';


function PostMenu({options, postId, postAuthor, postBody, postCategory, postTitle}) {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    
    {/*used to manage the state of the Dialog*/}
    const [isDialogOpen, setIsDialogOpen] = useState(false);

    {/*const that contained the data from the data layer*/}
    const [, dispatch] = useStateValue();

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    {/*Delete/Edit of post*/}
    const handleClickMenuIem = (event) =>{
        
      {/*here we delete the post and remove it from the dataLayer*/}
      if(event.target.textContent === "Delete"){
          fetch(`http://localhost:3001/posts/${postId}`, 
              {
                  method: 'DELETE',
                  headers: { 'Content-Type': 'application/json', 'Authorization': 'whatever-you-want' }
              }
          )
          .then(response => { 
              {/*We send the post id so we can remove the intended post from the data layer*/}
              dispatch({
                  type: actionTypes.REMOVEPOST,
                  data: postId
              });
              
          })
          .catch(error => {
              alert(error);
          });

          setAnchorEl(null);
      }

      {/*here we open the 'PostDialog' component so the post can be edited*/}
      if(event.target.textContent === "Edit"){
        
        setIsDialogOpen(true);
      }
       
    };

    {/*
        Callback function send to the PostDialog component
        this will update the 'isDialogOpen' state to false
    */}
    const handleDialogClose = () => {
        setIsDialogOpen(false);
    };

    return (
      <div>
        <IconButton
          aria-label="more"
          aria-controls="long-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          PaperProps={{
            style: {
              width: '20ch',
            },
          }}
        >
          {options.map((menuItem) => (
            <MenuItem key={menuItem} onClick={handleClickMenuIem}>
              {menuItem}
            </MenuItem>
          ))}
        </Menu>

        {
          (isDialogOpen === true)&&(
            <PostDialog
              creatingNewPost={false} 
              callBackDialogClose={handleDialogClose} 
              openState={isDialogOpen} 
              postDialogTitle={'Edit Post'}
              titleValue={postTitle}
              authorValue={postAuthor}
              bodyValue={postBody}
              categoryValue={postCategory}
              idValue={postId}
            ></PostDialog>
          )
        }

      </div>
    );
}

export default PostMenu
