import React from 'react'
import '../../Containers/Categories/Categories.css'
import Category from '../../Components/Category/Category'
import { useStateValue } from '../../Context/stateProvider'

function Categories() {
    let i = 0;
    const [ state, dispatch] = useStateValue();
    
    return (
        <div className="categories">
            <Category key={i++} Key={'All'} Label={'All'}></Category>
            {
                state.categories?.map((data) => (
                    <Category key={i++} Key={data.name} Label={data.name}></Category>
                ))
            } 
        </div>
    )
}

export default Categories 
