import React, { useState } from 'react';
import '../Post/Post.css';
import { Button, makeStyles, TextareaAutosize, TextField } from "@material-ui/core";
import { Card, CardActions, CardContent, CardHeader, IconButton, Menu, Typography} from "@material-ui/core";
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ThumbsUpDownIcon from '@material-ui/icons/ThumbsUpDown';
import ModeCommentIcon from '@material-ui/icons/ModeComment';
import PostMenu from '../PostMenu/PostMenu';
import { actionTypes } from '../../Context/reducer';
import { useStateValue } from '../../Context/stateProvider';
import Comments from '../../Containers/Comments/Comments';


{/*styles used on react components*/}
const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
      margin: theme.spacing(3)
    },
    avatar: {
      backgroundColor: '#82ccdd'
    },
    cardContent:{
      paddingBottom: '0px'
    },
    body:{
      marginBottom: '10px'
    },
    author:{
      borderBottomStyle: 'double'
    },
    cardActions:{
      padding: '16px'
    },
    positiveBalance:{
      marginLeft: '20px',
      marginRight: '12px',
      color: 'rgb(25, 145, 21)'
    },
    negativeBalance:{
      marginLeft: '20px',
      marginRight: '12px',
      color: 'rgb(95, 14, 14)'
    },
    neutralBalance:{
      marginLeft: '20px',
      marginRight: '12px',
    },
    comment:{
      marginLeft: 'auto',
      color: 'grey'
    },
    commentRoot:{
      padding: '16px'
    },
    commentTextArea:{
      marginBottom: '15px',
      marginTop: '27px',
      width: '100%'
    },
    commentSubmitButton:{
      float: 'right',
      paddingRight: '0px'
    }
  }));

//Component that contains the post structure and data
function Post({id, author, body, category, commentCount, deleted, timestamp, title, voteScore}) {
    const classes = useStyles();

    {/*
      Used so we can add a new comment to the current post
      Used so we can add alter the post voteCount
    */}
    const [, dispatch] = useStateValue();
    
    {/*manage state of upVote state*/}
    const [isUpVoteActive, setIsUpVoteActive] = useState(false);

    {/*manage state of upVote state*/}
    const [isDownVoteActive, setIsDownVoteActive] = useState(false);

    {/*manage state of CommentsDialog component*/}
    const [showComments, setShowComments] = useState(false);

    {/*Used to manage commentAuthor value*/}
    const [commentAuthor, setCommentAuthor] = useState('');

    {/*Used to manage author value*/}
    const [commentBody, setCommentBody] = useState('');
    

    {/*
        For creating UUID
        Information:
            https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid/2117523#2117523    
            Answer from @broofa's
    */}
    const uuidv4 = () => {
      return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
      );
  }
    
    {/*Up vote a post and update data layer or remove an up vote to 'neutral' state*/}
    const upVotePost = async () =>{
      let dispatchType;
      let requestOption;

      {/*
        In case the down vote was active we simulate the removal of it
        this way we aplly the up vote to the 'original' voteCount
      */}
      if(isDownVoteActive === true){
        setIsDownVoteActive(false);
        await voteCountUpdateAction("upVote", actionTypes.UPVOTEPOST);
      }
        
      
      if(isUpVoteActive === true){
        {/*If the post is already up voted we want to remove it and set to 'neutral' state*/} 
        setIsUpVoteActive(false);
        dispatchType = actionTypes.DOWNVOTEPOST;
        requestOption = "downVote";
      }
      else{
        {/*If is at the 'neutral' state we want to up vote*/} 
        setIsUpVoteActive(true);
        dispatchType = actionTypes.UPVOTEPOST;
        requestOption = "upVote"
      }

      await voteCountUpdateAction(requestOption, dispatchType);

    };

    {/*Down vote a post and update data layer or remove the down vote to 'neutral' state*/}
    const downVotePost = async () =>{
      let dispatchType;
      let requestOption;

      {/*
        In case the up vote was active we simulate the removal of it
        this way we aplly the down vote to the 'original' voteCount
      */}
      if(isUpVoteActive === true){
        setIsUpVoteActive(false);
        await voteCountUpdateAction("downVote", actionTypes.DOWNVOTEPOST);
      }
        
        
      
      
      if(isDownVoteActive === true){
        {/*If we already have a down vote then we want to remove it and set to 'neutral' state*/} 
        setIsDownVoteActive(false);
        dispatchType = actionTypes.UPVOTEPOST;
        requestOption = "upVote";
      }
      else{
        {/*If is at the 'neutral' state we want to up vote*/} 
        setIsDownVoteActive(true);
        dispatchType = actionTypes.DOWNVOTEPOST;
        requestOption = "downVote";
      }
        
      await voteCountUpdateAction(requestOption, dispatchType);
      
    };

    const voteCountUpdateAction = async (requestOption, dispatchType) =>{

      {/*Fetch to update vote count of post in server*/}
      await fetch(`http://localhost:3001/posts/${id}`, 
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'whatever-you-want' },
            body: JSON.stringify({option: requestOption})
        }
      )
      .then(response => { 
          {/*We send the post id so we can update the vote count in the data layer for the intended post */}
          dispatch({
              type: dispatchType,
              data: id
          });
      })
      .catch(error => {
          alert(error);
      });

    };
    
    {/*
        When the send comment button is clicked this is triggered 
        It will create/update the comment 
    */}
    const submitCommentClick = async () =>{

      let newComment ={
        id: uuidv4(),
        parentId : id,
        author: commentAuthor,
        body: commentBody,
        timestamp: new Date().getTime(),
        voteScore: 0
      }

        {/*Fetch to add the new comment in server*/}
        await fetch(`http://localhost:3001/comments`, 
          {
            method: 'POST', 
            headers: { 'Content-Type': 'application/json', 'Authorization': 'whatever-you-want' },
            body: JSON.stringify(newComment)
          }
        )
        .then(response => { 
            {/*Add the new comment to the data Layer*/}
            dispatch({
                type: actionTypes.ADDCOMMENT,
                data: newComment
            });

            setCommentAuthor('');
            setCommentBody('');
        })
        .catch(error => {
            alert(error);
        });

    };
    
    return (
      <div>
         <Card className={classes.root}>
          <CardHeader
            action={
              <PostMenu options={['Edit', 'Delete']} postId={id} postAuthor={author} postBody={body} postCategory={category} postTitle={title}  ></PostMenu>
            }
            title={title}
            subheader={new Date(timestamp).toLocaleString()}
          />
          <CardContent className={classes.cardContent}>
            <Typography className={classes.body} variant="body1" color="textPrimary" component="p" >
            {body}
            </Typography>
            <Typography className={classes.author} variant="overline" color="textSecondary" component="p">
            BY: {author}
            </Typography>
          </CardContent>
          <CardActions className={classes.cardActions} disableSpacing>
            <IconButton aria-label="Like" onClick={upVotePost}>
              <ThumbUpAltIcon style={{color: (isUpVoteActive===true)?("#008000"):("#808080")}}/>
            </IconButton>
            <IconButton aria-label="Dislike" onClick={downVotePost}>
              <ThumbDownIcon style={{color: (isDownVoteActive===true)?("#FF0000"):("#808080")}}/>
            </IconButton>
            {
              (voteScore > 0)?(
                <ThumbsUpDownIcon className={classes.positiveBalance}/>
              ):
              (voteScore < 0)?(
                <ThumbsUpDownIcon className={classes.negativeBalance}/>
              ):
              (
                <ThumbsUpDownIcon className={classes.neutralBalance}/>
              )
            }
            <span>{voteScore}</span>
            <IconButton className={classes.comment} onClick={() => setShowComments(!showComments)}>
              <ModeCommentIcon/>
            </IconButton>
            <span>{commentCount}</span>
          </CardActions>

          {
            (showComments === true)&&(
              <div>
                <Comments postId={id}></Comments>
                <div className={classes.commentRoot}>
                {/* Section destined to add new comments and edite */}
                  <TextField
                      // ref="TFAuthor"
                      id="TFAuthor"
                      autoFocus
                      margin="dense"
                      label="Author"
                      //disabled={!creatingNewPost}
                      fullWidth
                      value={commentAuthor}
                      onChange={(event) => setCommentAuthor(event.target.value)}
                      required={true}
                  />
                  <TextareaAutosize
                      id="TFBody"
                      value={commentBody}
                      className={classes.commentTextArea} 
                      rowsMin={3} 
                      rowsMax={10} 
                      placeholder="Content*"
                      onChange={(event) => setCommentBody(event.target.value)}
                  />
                  <Button className={classes.commentSubmitButton} type="submit" onClick={() =>  submitCommentClick()} color="primary">
                      Submit
                  </Button>
                </div>
              </div>
            )
          }

        </Card>
      </div>
    )
}

export default Post
